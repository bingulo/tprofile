EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Mini DaQ"
Date "2021-06-08"
Rev "1.0"
Comp "USP - FFCLRP - Departamento de Física"
Comment1 "Autor: Carlos Eduardo Gallo Filho (gallocarloseduardo@usp.br)"
Comment2 "Licença: GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)"
Comment3 "Circuito p/ automação da caracterização de transístores de efeito de campo orgânicos"
Comment4 ""
$EndDescr
$Comp
L Amplifier_Instrumentation:INA128 U4
U 1 1 60E2652C
P 3600 1675
F 0 "U4" H 3750 1800 50  0000 L CNN
F 1 "INA333" H 3750 1575 50  0000 L CNN
F 2 "Package_SO:MSOP-8_3x3mm_P0.65mm" H 3700 1675 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ina128.pdf" H 3700 1675 50  0001 C CNN
	1    3600 1675
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Instrumentation:INA128 U5
U 1 1 60E296A8
P 3600 2500
F 0 "U5" H 3750 2625 50  0000 L CNN
F 1 "INA333" H 3750 2400 50  0000 L CNN
F 2 "Package_SO:MSOP-8_3x3mm_P0.65mm" H 3700 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ina128.pdf" H 3700 2500 50  0001 C CNN
	1    3600 2500
	1    0    0    -1  
$EndComp
$Comp
L Analog_ADC:ADS1115IDGS U8
U 1 1 60E2AA33
P 4775 1875
F 0 "U8" H 4400 2200 50  0000 C CNN
F 1 "ADS1115IDGS" H 4425 2325 50  0000 C CNN
F 2 "Package_SO:MSOP-10_3x3mm_P0.5mm" H 4775 1375 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ads1113.pdf" H 4725 975 50  0001 C CNN
	1    4775 1875
	1    0    0    -1  
$EndComp
$Comp
L Analog_Switch:CD4052B U7
U 1 1 60E2BB7C
P 4750 3150
F 0 "U7" H 4600 3900 50  0000 C CNN
F 1 "CD4052B" H 4575 3800 50  0000 C CNN
F 2 "Package_SO:SOP-16_3.9x9.9mm_P1.27mm" H 4900 2400 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4052b.pdf" H 4730 3350 50  0001 C CNN
	1    4750 3150
	-1   0    0    1   
$EndComp
$Comp
L Amplifier_Operational:LM358 U6
U 1 1 60E5A1C8
P 3625 3425
F 0 "U6" H 3650 3175 50  0000 C CNN
F 1 "LM358" H 3675 3275 50  0000 C CNN
F 2 "Package_SO:SOP-8_3.76x4.96mm_P1.27mm" H 3625 3425 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 3625 3425 50  0001 C CNN
	1    3625 3425
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Instrumentation:INA326 U2
U 1 1 60E62AD0
P 2425 2475
F 0 "U2" H 2725 2250 50  0000 L CNN
F 1 "INA326" H 2650 2350 50  0000 L CNN
F 2 "Package_SO:MSOP-8_3x3mm_P0.65mm" H 2425 2475 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/ina326.pdf" H 2525 2475 50  0001 C CNN
	1    2425 2475
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Instrumentation:INA326 U3
U 1 1 60E6360F
P 2525 3375
F 0 "U3" H 2850 3175 50  0000 L CNN
F 1 "INA326" H 2750 3275 50  0000 L CNN
F 2 "Package_SO:MSOP-8_3x3mm_P0.65mm" H 2525 3375 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/ina326.pdf" H 2625 3375 50  0001 C CNN
	1    2525 3375
	1    0    0    -1  
$EndComp
$Comp
L Analog_DAC:MCP4728 U1
U 1 1 60E63F2A
P 2400 1600
F 0 "U1" H 2100 1950 50  0000 C CNN
F 1 "MCP4728" H 2100 1850 50  0000 C CNN
F 2 "Package_SO:MSOP-10_3x3mm_P0.5mm" H 2400 1000 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/22187E.pdf" H 2400 1850 50  0001 C CNN
	1    2400 1600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
