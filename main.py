from fets import *
import numpy as np

# Prefira usar f"{variável}"

def getScale(tipo, pin):
    if tipo == "V":
        scale = input(
            "Insira a escala para a tensão (%s) (1 -> V | 2 -> mV | 3 -> uV): "
            % pin
        )
    elif tipo == "I":
        scale = input(
            "Insira a escala para a corrente (%s) (1 -> A | 2 -> mA | 3 -> uA | 4 -> nA): "
            % pin
        )
    return 1000 ** (int(scale) - 1)

def getShunt(pin):
    res = int(input("Insira o fundo de escala p/ %s (1 -> 1mA | 2 -> 10uA | 3 -> 10nA): " % pin))
    return R_SHUNT[res]

# Possiveis data_rate: 8, 16, 32, 64, 128, 250, 475 e 860
R_SHUNT = [0, 10, 1008, 1000000]
MUX_PINS = {'A': 22, 'B': 23, 'INH': 24} 
DATA_RATE = 475
GAIN = 1
AMS = 100

r_shunt = {}
r_shunt['gate'] = getShunt('gate')
r_shunt['drain'] = getShunt('drain')

scale = {}
scale['Igate'] = getScale("I", 'gate')
scale['Idrain'] = getScale("I", 'drain')
scale['Vgate'] = getScale("V", 'gate')
scale['Vdrain'] = getScale("V", 'drain')

sleep_time = int(input("Insira o tempo de espera em segundos: "))

opc_caracterizacao = "1 -> Igs/Vgs | 2 -> Ids/Vds"
caracterize_type = int(
    input("Insira o tipo de caracterização que deseja(%s): " % opc_caracterizacao)
)

if caracterize_type == 1:
    x_type = "Vgs"
    x_pin = "gate"
    curve_type = "Vds"
    curve_pin = "drain"
elif caracterize_type == 2:
    x_type = "Vds"
    x_pin = "drain"
    curve_type = "Vgs"
    curve_pin = "gate"

x_inf, x_sup = input(
    "Insira os limites inferior e superior p/ %s (na escala selecionada!): " % x_type
).split()
x_qtd = int(input("Insira a quantidade de pontos p/ %s: " % x_type))
curve_inf, curve_sup = input(
    "Insira os limites inferior e superior p/ %s (na escala selecionada!): "
    % curve_type
).split()
curve_qtd = int(input("Insira a quantidade de pontos p/ %s: " % curve_type))

x_inf, x_sup = int(x_inf), int(x_sup)
curve_inf, curve_sup = int(curve_inf), int(curve_sup)

voltages_x = np.linspace(x_inf / scale["V"+x_pin],
                         x_sup / scale["V"+x_pin],
                         x_qtd)

voltages_curve = np.linspace(curve_inf / scale["V"+curve_pin],
                             curve_sup / scale["V"+curve_pin],
                             curve_qtd)

device = profiler(
    MUX_PINS,
    data_rate=DATA_RATE,
    gain=GAIN,
    AMS=AMS,
    r_shunt=r_shunt,
    sleep_time=sleep_time,
)

device.caracterize(
    voltages_x,
    voltages_curve,
    x_pin,
    curve_pin,
    scale = scale
)
